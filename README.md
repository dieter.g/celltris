# Celltris

I wanted to see whether it would be possible to create a Tetris-like game in Excel without needing any referencing. Created over the span of Christmas week, it turned out pretty okay - it has its problems and rotation can be done a lot better, but I was happy with the end result as this was just an experiment, not a full blown project after all.